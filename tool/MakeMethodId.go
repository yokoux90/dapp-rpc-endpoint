package tool

import (
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
)

func MakeMethodId(methodName string, abiStr string) (string, error) {
	abiPtr := &abi.ABI{}
	err := abiPtr.UnmarshalJSON([]byte(abiStr))
	if err != nil {
		return "", err
	}

	method := abiPtr.Methods[methodName]
	methodIdBytes := method.ID
	methodId := "0x" + common.Bytes2Hex(methodIdBytes)
	return methodId, nil
}
