package eth

import (
	"fmt"
	"github.com/ethereum/go-ethereum/rpc"
)

type ETHRPCClient struct {
	RpcUrl string
	client *rpc.Client
}

func NewETHRPCClient(rpcUrl string) *ETHRPCClient {
	client := &ETHRPCClient{
		RpcUrl: rpcUrl,
	}
	client.initRpc()
	return client
}

func (c *ETHRPCClient) initRpc() {
	rpcClient, err := rpc.DialHTTP(c.RpcUrl)
	if err != nil {
		errInfo := fmt.Errorf("Initialize rpc client failed: %s", err.Error()).Error()
		panic(errInfo)
	}
	c.client = rpcClient
}

func (c *ETHRPCClient) GetRpcClient() *rpc.Client {
	if c.client == nil {
		c.initRpc()
	}
	return c.client
}
