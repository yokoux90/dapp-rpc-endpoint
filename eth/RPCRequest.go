package eth

import (
	"dapp/constant"
	"dapp/model"
	"dapp/tool"
	"errors"
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/rlp"
	"github.com/ethereum/go-ethereum/rpc"
	"math/big"
	"strconv"
)

type ETHRPCRequest struct {
	nonceManager *NonceManager
	client       *ETHRPCClient
}

func NewETHRPCRequest(rpcUrl string) *ETHRPCRequest {
	request := &ETHRPCRequest{}
	request.nonceManager = NewNonceManager()
	request.client = NewETHRPCClient(rpcUrl)
	return request
}

// Web3ClientVersion
// @description Returns the current client version.
// @return  String - The current client version
func (r *ETHRPCRequest) Web3ClientVersion() (string, error) {
	methodName := constant.JsonRpcMethods.Web3ClientVersion

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result, err
}

// NetVersion
// @description Returns the current network id.
// @return  String - The current network id.
//				    "1": Ethereum Mainnet
//				    "3": Ropsten Testnet
//				    "4": Rinkeby Testnet
//				    "42": Kovan Testnet
//					...
func (r *ETHRPCRequest) NetVersion() (string, error) {
	methodName := constant.JsonRpcMethods.NetVersion

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result, err
}

// NetListening
// @description Returns true if client is actively listening for network connections.
// @return   Boolean - true when listening, otherwise false.
func (r *ETHRPCRequest) NetListening() (bool, error) {
	methodName := constant.JsonRpcMethods.NetVersion

	var result interface{}
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result.(bool), err
}

// NetPeerCount
// @description Returns number of peers currently connected to the client.
// @return   QUANTITY - integer of the number of connected peers.
func (r *ETHRPCRequest) NetPeerCount() (string, error) {
	methodName := constant.JsonRpcMethods.NetPeerCount

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result, err
}

// EthProtocolVersion
// @description Returns the current ethereum protocol version.
// @return String - The current ethereum protocol version
func (r *ETHRPCRequest) EthProtocolVersion() (string, error) {
	methodName := constant.JsonRpcMethods.EthProtocolVersion

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result, err
}

// EthSyncing
// @description Returns an object with data about the sync status or false.
// @return Object|Boolean, An object with sync status data or FALSE, when not syncing:
//         startingBlock: QUANTITY - The block at which the import started (will only be reset, after the sync reached his head)
//         currentBlock: QUANTITY - The current block, same as eth_blockNumber
//         highestBlock: QUANTITY - The estimated highest block
func (r *ETHRPCRequest) EthSyncing() (bool, error) {
	methodName := constant.JsonRpcMethods.EthSyncing

	var result interface{}
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result.(bool), err
}

// EthCoinbase
// @description Returns the client coinbase address.
// @return DATA, 20 bytes - the current coinbase address.
func (r *ETHRPCRequest) EthCoinbase() (string, error) {
	methodName := constant.JsonRpcMethods.EthCoinbase

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result, err
}

// EthMining
// @description Returns true if client is actively mining new blocks.
// @return Boolean - returns true of the client is mining, otherwise false.
func (r *ETHRPCRequest) EthMining() (bool, error) {
	methodName := constant.JsonRpcMethods.EthMining

	var result interface{}
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result.(bool), err
}

// EthChainId
// @description Returns the currently configured chain id, a value used in replay-protected transaction signing as introduced by EIP-155.
// @return QUANTITY - big integer of the current chain id.
func (r *ETHRPCRequest) EthChainId() (string, error) {
	methodName := constant.JsonRpcMethods.EthChainId

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result, err
}

// ParityNextNode
// @description Returns next available nonce for transaction from given account. Includes pending block and transaction queue.
//              NOTE: this method is only supported on the kovan network through our HTTPS and WebSocket endpoints.
// @params ADDRESS [required] - a string representing the address (20 bytes) to check for transaction count for
// @return TRANSACTION COUNT - a hex code of the integer representing the number of transactions sent from this address
func (r *ETHRPCRequest) ParityNextNode(address string) (string, error) {
	methodName := constant.JsonRpcMethods.ParityNextNode

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, address)
	return result, err
}

// EthHashRate
// @description Returns the number of hashes per second that the node is mining with.
// @return QUANTITY - number of hashes per second.
func (r *ETHRPCRequest) EthHashRate() (string, error) {
	methodName := constant.JsonRpcMethods.EthHashRate

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result, err
}

// EthGasPrice
// @description Returns the current price per gas in wei.
// @return QUANTITY - integer of the current gas price in wei.
func (r *ETHRPCRequest) EthGasPrice() (string, error) {
	methodName := constant.JsonRpcMethods.EthGasPrice

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result, err
}

// EthAccounts
// @description Returns a list of addresses owned by client.
// @return Array of DATA, 20 Bytes - addresses owned by the client.
func (r *ETHRPCRequest) EthAccounts() ([]string, error) {
	methodName := constant.JsonRpcMethods.EthAccounts

	var result []string
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result, err
}

// EthGetLatestBlockNumber
// @description Returns the number of most recent block.
// @return QUANTITY - integer of the current block number the client is on.
func (r *ETHRPCRequest) EthGetLatestBlockNumber() (string, error) {
	methodName := constant.JsonRpcMethods.EthBlockNumber

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result, err
}

// EthGetBalance
// @description Returns the balance of the account of given address.
// @params  DATA, 20 Bytes - address to check for balance.
//          QUANTITY|TAG - integer block number, or the string "latest", "earliest" or "pending"
// @return  QUANTITY - integer of the current balance in wei.
func (r *ETHRPCRequest) EthGetBalance(address string, tag string) (string, error) {
	methodName := constant.JsonRpcMethods.EthGetBalance

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, address, tag)

	return result, err
}

// EthGetBalances
// @description Returns the balance list of specificed addresses.
// @params  Array - list of address string
//          QUANTITY|TAG - integer block number, or the string "latest", "earliest" or "pending"
// @return  Array - the list of balance.
func (r *ETHRPCRequest) EthGetBalances(addresses []string, tag string) ([]*string, error) {
	methodName := constant.JsonRpcMethods.EthGetBalance

	var results []*string
	size := len(addresses)
	var requests []rpc.BatchElem

	for i := 0; i < size; i++ {
		var result string
		request := rpc.BatchElem{
			Method: methodName,
			Args:   []interface{}{addresses[i], tag},
			Result: &result,
		}

		requests = append(requests, request)
		results = append(results, &result)
	}

	err := r.client.GetRpcClient().BatchCall(requests)
	if err != nil {
		return nil, err
	}

	for _, requestErr := range requests {
		if requestErr.Error != nil {
			return nil, requestErr.Error
		}
	}

	//var finalResults []string
	//for _, res := range results {
	//	decimal, _ := new(big.Int).SetString(res[2:], 16)
	//	finalResults = append(finalResults, decimal.String())
	//}

	return results, err

}

// EthGetERC20Balance
// @description Returns the ERC20 balance from a contract.
// @params  Array see {model.ERC20BalanceRpcRequest}
//          QUANTITY|TAG - integer block number, or the string "latest", "earliest" or "pending"
// @return  Array - the list of balance.
func (r *ETHRPCRequest) EthGetERC20Balance(params []model.ERC20BalanceRpcRequest, tag string) ([]*string, error) {
	methodName := constant.JsonRpcMethods.EthCall

	methodId := "0x70a08231"

	var results []*string
	size := len(params)
	var requests []rpc.BatchElem

	for i := 0; i < size; i++ {
		var result string
		arg := &model.CallArgs{}
		userAddress := params[i].UserAddress
		arg.To = params[i].ContractAddress
		arg.Data = methodId + "000000000000000000000000" + userAddress[2:]

		request := rpc.BatchElem{
			Method: methodName,
			Args:   []interface{}{arg, "latest"},
			Result: &result,
		}

		requests = append(requests, request)
		results = append(results, &result)
	}

	err := r.client.GetRpcClient().BatchCall(requests)
	if err != nil {
		return nil, err
	}

	for _, requestErr := range requests {
		if requestErr.Error != nil {
			return nil, requestErr.Error
		}
	}

	return results, nil
}

// EthGetStorageAt
// @description Returns the value from a storage position at a given address.
// @params  DATA, 20 Bytes - address of the storage.
//          QUANTITY - integer of the position in the storage.
//          QUANTITY|TAG - integer block number, or the string "latest", "earliest" or "pending"
// @return  DATA - the value at this storage position.
func (r *ETHRPCRequest) EthGetStorageAt(address string, position string, tag string) (string, error) {
	methodName := constant.JsonRpcMethods.EthGetStorageAt

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, address, position, tag)
	return result, err
}

// ETHGetNonce
// @description Returns the number of transactions sent from an address.
// @params  DATA, 20 Bytes - address to check for balance.
//          QUANTITY|TAG - integer block number, or the string "latest", "earliest" or "pending"
// @return  QUANTITY - integer of the number of transactions send from this address.
func (r *ETHRPCRequest) ETHGetNonce(address string, tag string) (string, error) {
	methodName := constant.JsonRpcMethods.EthGetTransactionCount

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, address, tag)
	return result, err
}

// EthGetBlockTransactionCountByHash
// @description Returns the number of transactions in a block from a block matching the given block hash.
// @params  DATA, 32 Bytes - hash of a block
// @return  QUANTITY - integer of the number of transactions in this block.
func (r *ETHRPCRequest) EthGetBlockTransactionCountByHash(blockHash string) (string, error) {
	methodName := constant.JsonRpcMethods.EthGetBlockTransactionCountByHash

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, blockHash)
	return result, err
}

// EthGetBlockTransactionCountByNumber
// @description Returns the number of transactions in a block matching the given block number.
// @params  QUANTITY|TAG - integer of a block number, or the string "earliest", "latest" or "pending"
// @return  QUANTITY - integer of the number of transactions in this block.
func (r *ETHRPCRequest) EthGetBlockTransactionCountByNumber(blockNumber string) (string, error) {
	methodName := constant.JsonRpcMethods.EthGetBlockTransactionCountByNumber

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, blockNumber)
	return result, err
}

// EthGetUncleCountByBlockHash
// @description Returns the number of uncles in a block from a block matching the given block hash.
// @params  DATA, 32 Bytes - hash of a block
// @return  QUANTITY - integer of the number of uncles in this block.
func (r *ETHRPCRequest) EthGetUncleCountByBlockHash(blockHash string) (string, error) {
	methodName := constant.JsonRpcMethods.EthGetUncleCountByBlockHash

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, blockHash)
	return result, err
}

// EthGetUncleCountByBlockNumber
// @description Returns the number of uncles in a block from a block matching the given block number.
// @params  QUANTITY|TAG - integer of a block number, or the string "earliest", "latest" or "pending"
// @return  QUANTITY - integer of the number of uncles in this block.
func (r *ETHRPCRequest) EthGetUncleCountByBlockNumber(blockNumber string) (string, error) {
	methodName := constant.JsonRpcMethods.EthGetUncleCountByBlockNumber

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, blockNumber)
	return result, err
}

// EthGetCode
// @description Returns code at a given address.
// @params  DATA, 20 Bytes - address to check for balance.
//          QUANTITY|TAG - integer block number, or the string "latest", "earliest" or "pending"
// @return  DATA - the code from the given address.
func (r *ETHRPCRequest) EthGetCode(address string, tag string) (string, error) {
	methodName := constant.JsonRpcMethods.EthGetCode

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, address, tag)
	return result, err
}

// EthSign
// @description The sign method calculates an Ethereum specific signature with: sign(keccak256("\x19Ethereum Signed Message:\n" + len(message) + message))).
//              By adding a prefix to the message makes the calculated signature recognisable as an Ethereum specific signature. This prevents misuse where a malicious DApp can sign arbitrary data (e.g. transaction) and use the signature to impersonate the victim.
//              Note the address to sign with must be unlocked.
// @params  DATA, 20 Bytes - address to check for balance.
//          DATA, N Bytes - message to sign
// @return  DATA - Signature
func (r *ETHRPCRequest) EthSign(address string, message string) (string, error) {
	methodName := constant.JsonRpcMethods.EthSign

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, address, message)
	return result, err
}

// EthSendTransaction
// @description Creates new message call transaction or a contract creation, if the data field contains code.
// @params  Object - The transaction object
//               from: DATA, 20 Bytes - The address the transaction is sent from.
//               to: DATA, 20 Bytes - (optional when creating new contract) The address the transaction is directed to.
//               gas: QUANTITY - (optional, default: 90000) Integer of the gas provided for the transaction execution. It will return unused gas.
//               gasPrice: QUANTITY - (optional, default: To-Be-Determined) Integer of the gasPrice used for each paid gas, in Wei.
//               value: QUANTITY - (optional) Integer of the value sent with this transaction, in Wei.
//               data: DATA - The compiled code of a contract OR the hash of the invoked method signature and encoded parameters. For details see Ethereum Contract ABI.
//               nonce: QUANTITY - (optional) Integer of a nonce. This allows to overwrite your own pending transactions that use the same nonce.
// @return  DATA, 32 Bytes - the transaction hash, or the zero hash if the transaction is not yet available.
func (r *ETHRPCRequest) EthSendTransaction(transaction model.CallArgs) (string, error) {
	if !common.IsHexAddress(transaction.From) || !common.IsHexAddress(transaction.To) {
		return "", errors.New("invalid address")
	}

	to := common.HexToAddress(transaction.To)
	gasPrice := new(big.Int).SetUint64(transaction.GasPrice)

	realV := tool.GetRealDecimalValue(transaction.Value, 18)
	if realV == "" {
		return "", errors.New("invalid value")
	}
	amount, _ := new(big.Int).SetString(realV, 10)

	nonce := r.nonceManager.GetNonce(transaction.From)
	if nonce == nil {
		n, err := r.ETHGetNonce(transaction.From, "latest")
		if err != nil {
			return "", fmt.Errorf("fetch nonce failed: %s", err.Error())
		}
		un, _ := strconv.ParseUint(n, 10, 64)
		nonce = new(big.Int).SetUint64(un)
		r.nonceManager.SetNonce(transaction.From, nonce)
	}

	data := []byte("")

	tx := types.NewTx(
		&types.LegacyTx{
			Nonce:    nonce.Uint64(),
			To:       &to,
			Value:    amount,
			Gas:      transaction.Gas,
			GasPrice: gasPrice,
			Data:     data,
		})

	txData, err := SignETHTransaction(transaction.From, tx)
	if err != nil {
		return "", err
	}

	txRlpData, err := rlp.EncodeToBytes(txData)
	if err != nil {
		return "", err
	}

	return r.EthSendTx("0x" + common.Bytes2Hex(txRlpData))
}

// EthSendTx
// @description Call send transaction.
// @params  txRlpData - String
// @return  DATA, 32 Bytes - the transaction hash, or the zero hash if the transaction is not yet available.
func (r *ETHRPCRequest) EthSendTx(txRlpData string) (string, error) {
	methodName := constant.JsonRpcMethods.EthSendRawTransaction

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, txRlpData)
	return result, err
}

// EthSendERC20Transaction
// @description Creates new message call transaction or a contract creation, if the data field contains code.
// @params       from: DATA, 20 Bytes - The address the transaction is sent from.
//               contract: DATA, 20 Bytes - The address of contract
//               to: DATA, 20 Bytes - The address the transaction is directed to.
//               gas: QUANTITY - (optional, default: 90000) Integer of the gas provided for the transaction execution. It will return unused gas.
//               gasPrice: QUANTITY - (optional, default: To-Be-Determined) Integer of the gasPrice used for each paid gas, in Wei.
//               valueStr: QUANTITY - (optional) Integer of the value sent with this transaction, in Wei.
//               decimal: int
// @return  DATA, 32 Bytes - the transaction hash, or the zero hash if the transaction is not yet available.
func (r *ETHRPCRequest) EthSendERC20Transaction(from, contract, receiver, valueStr string, gas, gasPrice uint64, decimal int) (string, error) {
	if !common.IsHexAddress(from) || !common.IsHexAddress(contract) || !common.IsHexAddress(receiver) {
		return "", errors.New("invalid address")
	}

	to := common.HexToAddress(contract)
	gasPrice_ := new(big.Int).SetUint64(gasPrice)

	amount := new(big.Int).SetInt64(0)

	nonce := r.nonceManager.GetNonce(from)
	if nonce == nil {
		n, err := r.ETHGetNonce(from, "latest")
		if err != nil {
			return "", fmt.Errorf("fetch nonce failed: %s", err.Error())
		}
		un, _ := strconv.ParseUint(n, 10, 64)
		nonce = new(big.Int).SetUint64(un)
		r.nonceManager.SetNonce(from, nonce)
	}

	data := BuildERC20Transaction(valueStr, receiver, decimal)
	dataBytes := common.FromHex(data)

	tx := types.NewTx(
		&types.LegacyTx{
			Nonce:    nonce.Uint64(),
			To:       &to,
			Value:    amount,
			Gas:      gas,
			GasPrice: gasPrice_,
			Data:     dataBytes,
		})

	txData, err := SignETHTransaction(from, tx)
	if err != nil {
		return "", err
	}

	txRlpData, err := rlp.EncodeToBytes(txData)
	if err != nil {
		return "", err
	}

	return r.EthSendTx("0x" + common.Bytes2Hex(txRlpData))
}

// EthSendRawTransaction
// @description Creates new message call transaction or a contract creation for signed transactions.
// @params  DATA, The signed transaction data.
// @return  DATA, 32 Bytes - the transaction hash, or the zero hash if the transaction is not yet available.
func (r *ETHRPCRequest) EthSendRawTransaction(tx string) (string, error) {
	methodName := constant.JsonRpcMethods.EthSendRawTransaction

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, tx)
	return result, err
}

// EthCall
// @description Creates new message call transaction or a contract creation, if the data field contains code.
// @params  Object - The transaction object
//               from: DATA, 20 Bytes - The address the transaction is sent from.
//               to: DATA, 20 Bytes - (optional when creating new contract) The address the transaction is directed to.
//               gas: QUANTITY - (optional, default: 90000) Integer of the gas provided for the transaction execution. It will return unused gas.
//               gasPrice: QUANTITY - (optional, default: To-Be-Determined) Integer of the gasPrice used for each paid gas, in Wei.
//               value: QUANTITY - (optional) Integer of the value sent with this transaction, in Wei.
//               data: DATA - The compiled code of a contract OR the hash of the invoked method signature and encoded parameters. For details see Ethereum Contract ABI.
//               nonce: QUANTITY - (optional) Integer of a nonce. This allows to overwrite your own pending transactions that use the same nonce.
//          QUANTITY|TAG - integer block number, or the string "latest", "earliest" or "pending",
// @return  DATA - the return value of executed contract.
func (r *ETHRPCRequest) EthCall(transaction model.CallArgs, tag string) (string, error) {
	methodName := constant.JsonRpcMethods.EthCall

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, transaction, tag)
	return result, err
}

// EthEstimateGas
// @description Generates and returns an estimate of how much gas is necessary to allow the transaction to complete. The transaction will not be added to the blockchain. Note that the estimate may be significantly more than the amount of gas actually used by the transaction, for a variety of reasons including EVM mechanics and node performance.
// @params  Object - The transaction object {model.TransactionRequest}
//               from: DATA, 20 Bytes - The address the transaction is sent from.
//               to: DATA, 20 Bytes - (optional when creating new contract) The address the transaction is directed to.
//               gas: QUANTITY - (optional, default: 90000) Integer of the gas provided for the transaction execution. It will return unused gas.
//               gasPrice: QUANTITY - (optional, default: To-Be-Determined) Integer of the gasPrice used for each paid gas, in Wei.
//               value: QUANTITY - (optional) Integer of the value sent with this transaction, in Wei.
//               data: DATA - The compiled code of a contract OR the hash of the invoked method signature and encoded parameters. For details see Ethereum Contract ABI.
//               nonce: QUANTITY - (optional) Integer of a nonce. This allows to overwrite your own pending transactions that use the same nonce.
//          QUANTITY|TAG - integer block number, or the string "latest", "earliest" or "pending",
// @return  QUANTITY - the amount of gas used.
func (r *ETHRPCRequest) EthEstimateGas(transaction model.CallArgs, tag string) (string, error) {
	methodName := constant.JsonRpcMethods.EthEstimateGas

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, transaction, tag)
	return result, err
}

// EthGetBlockByHash
// @description Returns information about a block by hash.
// @params DATA, 32 Bytes - Hash of a block.
//         Boolean - If true it returns the full transaction objects, if false only the hashes of the transactions.
// @return {model.Block}
func (r *ETHRPCRequest) EthGetBlockByHash(txHash string, full bool) (model.Block, error) {
	methodName := constant.JsonRpcMethods.EthGetBlockByHash

	result := model.Block{}
	err := r.client.GetRpcClient().Call(&result, methodName, txHash, full)
	return result, err
}

// EthGetBlockByNumber
// @description Returns information about a block by block number.
// @params QUANTITY|TAG - integer of a block number, or the string "earliest", "latest" or "pending"
//         Boolean - If true it returns the full transaction objects, if false only the hashes of the transactions.
// @return {model.Block}
func (r *ETHRPCRequest) EthGetBlockByNumber(blockNum string, full bool) (model.Block, error) {
	methodName := constant.JsonRpcMethods.EthGetBlockByNumber

	result := model.Block{}
	err := r.client.GetRpcClient().Call(&result, methodName, blockNum, full)
	return result, err
}

// EthGetTransactionByHash
// @description Returns the information about a transaction requested by transaction hash.
// @params DATA, 32 Bytes - hash of a transaction
// @return {model.Transaction}
func (r *ETHRPCRequest) EthGetTransactionByHash(txHash string) (model.Transaction, error) {
	methodName := constant.JsonRpcMethods.EthGetTransactionByHash

	result := model.Transaction{}
	err := r.client.GetRpcClient().Call(&result, methodName, txHash)
	return result, err
}

// EthGetTransactionByBlockHashAndIndex
// @description Returns information about a transaction by block hash and transaction index position.
// @params DATA, 32 Bytes - hash of a block.
//         QUANTITY - integer of the transaction index position.
// @return {model.Transaction}
func (r *ETHRPCRequest) EthGetTransactionByBlockHashAndIndex(blockHash string, position string) (model.Transaction, error) {
	methodName := constant.JsonRpcMethods.EthGetTransactionByBlockHashAndIndex

	result := model.Transaction{}
	err := r.client.GetRpcClient().Call(&result, methodName, blockHash, position)
	return result, err
}

// EthGetTransactionByBlockNumberAndIndex
// @description Returns information about a transaction by block number and transaction index position.
// @params QUANTITY|TAG - a block number, or the string "earliest", "latest" or "pending"
//         QUANTITY - integer of the transaction index position.
// @return {model.Transaction}
func (r *ETHRPCRequest) EthGetTransactionByBlockNumberAndIndex(blockNum string, position string) (model.Transaction, error) {
	methodName := constant.JsonRpcMethods.EthGetTransactionByBlockNumberAndIndex

	result := model.Transaction{}
	err := r.client.GetRpcClient().Call(&result, methodName, blockNum, position)
	return result, err
}

// EthGetUncleByBlockHashAndIndex
// @description Returns information about a uncle of a block by hash and uncle index position.
// @params DATA, 32 Bytes - hash of a block
//         QUANTITY - The uncle’s index position.
// @return {model.Block}
func (r *ETHRPCRequest) EthGetUncleByBlockHashAndIndex(blockHash string, position string) (model.Block, error) {
	methodName := constant.JsonRpcMethods.EthGetUncleByBlockHashAndIndex

	result := model.Block{}
	err := r.client.GetRpcClient().Call(&result, methodName, blockHash, position)
	return result, err
}

// EthGetUncleByBlockNumberAndIndex
// @description Returns information about a uncle of a block by number and uncle index position.
// @params QUANTITY|TAG - a block number, or the string "earliest", "latest" or "pending"
//         QUANTITY - The uncle’s index position.
// @return {model.Block}
func (r *ETHRPCRequest) EthGetUncleByBlockNumberAndIndex(blockNum string, position string) (model.Block, error) {
	methodName := constant.JsonRpcMethods.EthGetUncleByBlockNumberAndIndex

	result := model.Block{}
	err := r.client.GetRpcClient().Call(&result, methodName, blockNum, position)
	return result, err
}

// EthNewBlockFilter
// @description Creates a filter in the node, to notify when a new block arrives.
//              To check if the state has changed, call eth_getFilterChanges.
// @return QUANTITY - A filter id.
func (r *ETHRPCRequest) EthNewBlockFilter() (string, error) {
	methodName := constant.JsonRpcMethods.EthNewBlockFilter

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result, err
}

// EthNewPendingTransactionFilter
// @description Creates a filter in the node, to notify when new pending transactions arrive.
//              To check if the state has changed, call eth_getFilterChanges.
// @return QUANTITY - A filter id.
func (r *ETHRPCRequest) EthNewPendingTransactionFilter() (string, error) {
	methodName := constant.JsonRpcMethods.EthNewPendingTransactionFilter

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result, err
}

// EthUninstallFilter
// @description Uninstalls a filter with given id. Should always be called when watch is no longer needed.
//              Additonally Filters timeout when they aren’t requested with eth_getFilterChanges for a period of time.
// @params QUANTITY - A filter id.
// @return Boolean - true if the filter was successfully uninstalled, otherwise false.
func (r *ETHRPCRequest) EthUninstallFilter(filterId string) (bool, error) {
	methodName := constant.JsonRpcMethods.EthUninstallFilter

	var result interface{}
	err := r.client.GetRpcClient().Call(&result, methodName, filterId)
	return result.(bool), err
}

// EthGetFilterChanges
// @description Polling method for a filter, which returns an array of logs which occurred since last poll.
// @params QUANTITY - A filter id.
// @return Array - Array of log objects, or an empty array if nothing has changed since last poll.
//				For filters created with eth_newBlockFilter the return are block hashes (DATA, 32 Bytes), e.g. ["0x3454645634534..."].
//				For filters created with eth_newPendingTransactionFilter  the return are transaction hashes (DATA, 32 Bytes), e.g. ["0x6345343454645..."].
//				For filters created with eth_newFilter logs are objects with following params:
//					removed: TAG - true when the log was removed, due to a chain reorganization. false if its a valid log.
//					logIndex: QUANTITY - integer of the log index position in the block. null when its pending log.
//					transactionIndex: QUANTITY - integer of the transactions index position log was created from. null when its pending log.
//					transactionHash: DATA, 32 Bytes - hash of the transactions this log was created from. null when its pending log.
//					blockHash: DATA, 32 Bytes - hash of the block where this log was in. null when its pending. null when its pending log.
//					blockNumber: QUANTITY - the block number where this log was in. null when its pending. null when its pending log.
//					address: DATA, 20 Bytes - address from which this log originated.
//					data: DATA - contains one or more 32 Bytes non-indexed arguments of the log.
//					topics: Array of DATA - Array of 0 to 4 32 Bytes DATA of indexed log arguments. (In solidity: The first topic is the hash of the signature of the event (e.g. Deposit(address,bytes32,uint256)), except you declared the event with the anonymous specifier.)
func (r *ETHRPCRequest) EthGetFilterChanges(filterId string) ([]interface{}, error) {
	methodName := constant.JsonRpcMethods.EthGetFilterChanges

	var result []interface{}
	err := r.client.GetRpcClient().Call(&result, methodName, filterId)
	return result, err
}

// EthGetFilterLogs
// @description Returns an array of all logs matching filter with given id.
// @params QUANTITY - A filter id.
// @return See {EthGetFilterChanges}
func (r *ETHRPCRequest) EthGetFilterLogs(filterId string) ([]interface{}, error) {
	methodName := constant.JsonRpcMethods.EthGetFilterLogs

	var result []interface{}
	err := r.client.GetRpcClient().Call(&result, methodName, filterId)
	return result, err
}

// EthGetWork
// @description Returns the hash of the current block, the seedHash, and the boundary condition to be met (“target”).
// @return Array - Array with the following properties:
//				DATA, 32 Bytes - current block header pow-hash
//				DATA, 32 Bytes - the seed hash used for the DAG.
//				DATA, 32 Bytes - the boundary condition (“target”), 2^256 / difficulty.
func (r *ETHRPCRequest) EthGetWork() ([]string, error) {
	methodName := constant.JsonRpcMethods.EthGetWork

	var result []string
	err := r.client.GetRpcClient().Call(&result, methodName)
	return result, err
}

// EthSubmitWork
// @description Used for submitting a proof-of-work solution.
// @params DATA, 8 Bytes - The nonce found (64 bits)
//		   DATA, 32 Bytes - The header’s pow-hash (256 bits)
//		   DATA, 32 Bytes - The mix digest (256 bits)
// @return Boolean - returns true if the provided solution is valid, otherwise false.
func (r *ETHRPCRequest) EthSubmitWork(nonce string, powHash string, digest string) (bool, error) {
	methodName := constant.JsonRpcMethods.EthSubmitWork

	var result interface{}
	err := r.client.GetRpcClient().Call(&result, methodName, nonce, powHash, digest)
	return result.(bool), err
}

// EthSubscribe
// https://docs.infura.io/infura/networks/ethereum/json-rpc-methods/subscription-methods/eth_subscribe
// @description Creates a new subscription over particular events. The node will return a subscription id. For each event that matches the subscription a notification with relevant data is sent together with the subscription id.
// @params SUBSCRIPTION TYPE NAME [required]
//             newHeads- Subscribing to this, fires a notification each time a new header is appended to the chain, including chain reorganizations. In case of a chain reorganization the subscription will emit all new headers for the new chain. Therefore the subscription can emit multiple headers on the same height.
//             logs - Returns logs that are included in new imported blocks and match the given filter criteria. In case of a chain reorganization previous sent logs that are on the old chain will be resend with the removed property set to true. Logs from transactions that ended up in the new chain are emitted. Therefore a subscription can emit logs for the same transaction multiple times.
//             		address (optional) - either an address or an array of addresses. Only logs that are created from these addresses are returned (optional)
//             		topics (optional) - only logs which match the specified topics (optional)
//             newPendingTransactions - Returns the hash for all transactions that are added to the pending state and are signed with a key that is available in the node. When a transaction that was previously part of the canonical chain isn't part of the new canonical chain after a reogranization its again emitted.
//             syncing - Indicates when the node starts or stops synchronizing. The result can either be a boolean indicating that the synchronization has started (true), finished (false) or an object with various progress indicators. NOT SUPPORTED ON KOVAN!
// @return SUBSCRIPTION ID - ID of the newly created subscription on the node
func (r *ETHRPCRequest) EthSubscribe(subscriptionType interface{}) (string, error) {
	methodName := constant.JsonRpcMethods.EthSubscribe

	var result string
	err := r.client.GetRpcClient().Call(&result, methodName, subscriptionType)
	return result, err
}

// EthUnsubscribe
// @description  Subscriptions are cancelled with a regular RPC call with eth_unsubscribe as method and the subscription id as first parameter. It returns a bool indicating if the subscription was cancelled successful.
// @params  SUBSCRIPTION ID [required]
// @return  UNSUBSCRIBED FLAG - true if the subscription was cancelled successful.
func (r *ETHRPCRequest) EthUnsubscribe(subscriptionId string) (bool, error) {
	methodName := constant.JsonRpcMethods.EthUnsubscribe

	var result interface{}
	err := r.client.GetRpcClient().Call(&result, methodName, subscriptionId)
	return result.(bool), err
}

// GetTransactions
// @description Get transactions by multiple hash values
// @params txHashs - Array of transaction hash string
// @return Array of transaction information
func (r *ETHRPCRequest) GetTransactions(txHashs []string) ([]*model.Transaction, error) {
	methodName := constant.JsonRpcMethods.EthGetTransactionByHash

	var results []*model.Transaction

	size := len(txHashs)

	var requests []rpc.BatchElem

	for i := 0; i < size; i++ {
		result := model.Transaction{}

		request := rpc.BatchElem{
			Method: methodName,
			Args:   []interface{}{txHashs[i]},
			Result: &result,
		}

		requests = append(requests, request)
		results = append(results, &result)
	}
	err := r.client.GetRpcClient().BatchCall(requests)
	return results, err
}
