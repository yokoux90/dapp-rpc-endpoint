package eth

import (
	"dapp/tool"
	"errors"
	"github.com/ethereum/go-ethereum/accounts"
	"github.com/ethereum/go-ethereum/accounts/keystore"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"math/big"
)

// CreateETHWallet
// @description Create a new wallet
// @params String - password / pincode
// @return String - wallet address
func CreateETHWallet(password string) (string, error) {
	if password == "" {
		return "", errors.New("password is required")
	}
	if len(password) < 4 {
		return "", errors.New("the length of password must more than 4 words")
	}
	keyDir := "./keystore"
	ks := keystore.NewKeyStore(keyDir, keystore.StandardScryptN, keystore.StandardScryptP)
	wallet, err := ks.NewAccount(password)
	if err != nil {
		return "0x", err
	}
	return wallet.Address.String(), nil
}

// ETHUnlockMap - Global variables to persist unlocked wallet
var ETHUnlockMap map[string]accounts.Account

// UnlockKeys - keystore instance
var UnlockKeys *keystore.KeyStore

func UnlockETHWallet(keyDir, address, password string) error {
	if UnlockKeys == nil {
		UnlockKeys = keystore.NewKeyStore(keyDir, keystore.StandardScryptN, keystore.StandardScryptP)
		if UnlockKeys == nil {
			return errors.New("keystore is nil")
		}
	}

	unlock := accounts.Account{Address: common.HexToAddress(address)}

	if err := UnlockKeys.Unlock(unlock, password); err != nil {
		return errors.New("unlock err: " + err.Error())
	}
	if ETHUnlockMap == nil {
		ETHUnlockMap = map[string]accounts.Account{}
	}
	ETHUnlockMap[address] = unlock
	return nil
}

func SignETHTransaction(address string, transaction *types.Transaction) (*types.Transaction, error) {
	if UnlockKeys == nil {
		return nil, errors.New("account was not unlocked")
	}
	account := ETHUnlockMap[address]
	if !common.IsHexAddress(account.Address.String()) {
		return nil, errors.New("account format is invalid")
	}
	return UnlockKeys.SignTx(account, transaction, nil)
}

func SignETHMessage(address string, message string) (string, error) {
	if UnlockKeys == nil {
		return "", errors.New("account was not unlocked")
	}
	account := ETHUnlockMap[address]
	if !common.IsHexAddress(account.Address.String()) {
		return "", errors.New("account format is invalid")
	}
	message_ := common.BytesToHash([]byte(message)).Bytes()
	signed, err := UnlockKeys.SignHash(account, message_)
	if err != nil {
		return "", err
	}
	return "0x" + common.Bytes2Hex(signed), nil
}

func BuildERC20Transaction(value, receiver string, decimal int) string {
	realValue := tool.GetRealDecimalValue(value, decimal)
	valueBig, _ := new(big.Int).SetString(realValue, 10)

	// TODO tool_test.go TestMakeMethodId()
	methodId := "0xbb4e3f4d"
	param1 := common.HexToHash(receiver).String()[2:]
	param2 := common.BytesToHash(valueBig.Bytes()).String()[2:]

	return methodId + param1 + param2
}
