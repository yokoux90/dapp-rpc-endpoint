package test

import (
	"dapp/eth"
	"dapp/model"
	"dapp/tool"
	"github.com/ethereum/go-ethereum/common"
	"testing"
)

var testContractABI string = `[
	{
		"inputs": [
			{
				"internalType": "uint8",
				"name": "arg1",
				"type": "uint8"
			},
			{
				"internalType": "uint8",
				"name": "arg2",
				"type": "uint8"
			}
		],
		"name": "add",
		"outputs": [
			{
				"internalType": "uint8",
				"name": "",
				"type": "uint8"
			}
		],
		"stateMutability": "pure",
		"type": "function"
	}
]`

func TestMakeMethodId(t *testing.T) {

	methodName := "add"
	methodId, err := tool.MakeMethodId(methodName, testContractABI)
	if err != nil {
		t.Error("Failed: ", err.Error())
		return
	}
	t.Log("methodId: ", methodId)
}

func TestEthCallContractMethod(t *testing.T) {
	methodName := "add"
	methodId, err := tool.MakeMethodId(methodName, testContractABI)
	tag := "latest"

	arg1 := common.HexToHash("2").String()[2:]
	arg2 := common.HexToHash("3").String()[2:]

	contractAddress := "0x2abb7989285b6c72e6067717f5e26697d1da3627"

	args := model.CallArgs{
		To:       contractAddress,
		Data:     methodId + arg1 + arg2,
		Gas:      0,
		GasPrice: 0,
		Value:    "0x0",
		Nonce:    "0x0",
	}

	result, err := eth.NewETHRPCRequest(rpcUrl).EthCall(args, tag)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestGetRootPath(t *testing.T) {
	dir := tool.GetRootPath()
	t.Log(dir)
}
