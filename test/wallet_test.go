package test

import (
	"dapp/eth"
	json2 "encoding/json"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"math/big"
	"testing"
)

var keyDir = "./keystores"

func TestCreateETHWallet(t *testing.T) {
	pincode := "1234"
	result, err := eth.CreateETHWallet(pincode)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestUnlockETHWallet(t *testing.T) {
	address := "0xDF7B186C2dee907Be7dD9295Da46e681b6FE9e4A"

	err := eth.UnlockETHWallet(keyDir, address, "1234")
	if err != nil {
		t.Error("Unlock failed" + err.Error())
	} else {
		t.Log("Unlock success")
	}

	// Test sign transaction
	tx := types.NewTx(
		&types.LegacyTx{
			Nonce:    123,
			To:       &common.Address{},
			Value:    new(big.Int).SetInt64(10),
			Gas:      1000,
			GasPrice: new(big.Int).SetInt64(20),
			Data:     []byte("hello"),
		},
	)

	signTx, err := eth.SignETHTransaction(address, tx)
	if err != nil {
		t.Error("sign failed: ", err.Error())
		return
	}
	data, _ := json2.Marshal(signTx)
	t.Log("sign succeed: ", string(data))
}

func TestSignMessage(t *testing.T) {
	address := "0xDF7B186C2dee907Be7dD9295Da46e681b6FE9e4A"

	err := eth.UnlockETHWallet(keyDir, address, "1234")
	if err != nil {
		t.Error("Unlock failed" + err.Error())
	} else {
		t.Log("Unlock success")
	}

	signTx, err := eth.SignETHMessage(address, "hello")
	if err != nil {
		t.Error("sign failed: ", err.Error())
		return
	}

	t.Log("sign succeed: ", signTx)
}
