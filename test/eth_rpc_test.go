package test

import (
	"dapp/eth"
	"dapp/model"
	json2 "encoding/json"
	"reflect"
	"strconv"
	"testing"
)

var rpcUrl = "https://ropsten.infura.io/v3/1c71b7f01d194ff985ca006da807a48f"

func TestETHRPCClient(t *testing.T) {

	client2 := eth.NewETHRPCClient(rpcUrl)
	if client2 == nil {
		t.Log("Initialize failed!!!")
	} else {
		t.Log("Initialize succeed!")
	}
}

func TestWeb3ClientVersion(t *testing.T) {
	result, err := eth.NewETHRPCRequest(rpcUrl).Web3ClientVersion()
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestNetVersion(t *testing.T) {
	result, err := eth.NewETHRPCRequest(rpcUrl).NetVersion()
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestNetListening(t *testing.T) {
	result, err := eth.NewETHRPCRequest(rpcUrl).NetListening()
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	if result {
		t.Log("client is actively listening for network connections")
	} else {
		t.Log("client is not actively listening for network connections")
	}
}

func TestNetPeerCount(t *testing.T) {
	result, err := eth.NewETHRPCRequest(rpcUrl).NetPeerCount()
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthProtocolVersion(t *testing.T) {
	result, err := eth.NewETHRPCRequest(rpcUrl).EthProtocolVersion()
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthSyncing(t *testing.T) {
	result, err := eth.NewETHRPCRequest(rpcUrl).EthSyncing()
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	if reflect.TypeOf(result).String() == "Bool" {
		t.Log(result)
	} else {
		json, _ := json2.Marshal(result)
		t.Log(string(json))
	}
}

func TestEthCoinbase(t *testing.T) {
	result, err := eth.NewETHRPCRequest(rpcUrl).EthCoinbase()
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthMining(t *testing.T) {
	result, err := eth.NewETHRPCRequest(rpcUrl).EthMining()
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	if result {
		t.Log("client is actively mining new blocks")
	} else {
		t.Log("client is not actively mining new blocks")
	}
}

func TestEthGasPrice(t *testing.T) {
	result, err := eth.NewETHRPCRequest(rpcUrl).EthGasPrice()
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthAccounts(t *testing.T) {
	result, err := eth.NewETHRPCRequest(rpcUrl).EthAccounts()
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthGetLatestBlockNumber(t *testing.T) {
	result, err := eth.NewETHRPCRequest(rpcUrl).EthGetLatestBlockNumber()
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthGetBalance(t *testing.T) {
	address := "0x704F4eDc12810f554A8Cc4509b278890De201272"
	tag := "latest"
	result, err := eth.NewETHRPCRequest(rpcUrl).EthGetBalance(address, tag)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthGetBalances(t *testing.T) {
	addresses := []string{
		"0x846820572ADF3D6BD50f5DAb347252340e9639E9",
		"0x704F4eDc12810f554A8Cc4509b278890De201272",
	}
	tag := "latest"
	result, err := eth.NewETHRPCRequest(rpcUrl).EthGetBalances(addresses, tag)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	json, _ := json2.Marshal(result)
	t.Log(string(json))
}

func TestEthGetERC20Balance(t *testing.T) {

	rpcUrl1 := "https://mainnet.infura.io/v3/1c71b7f01d194ff985ca006da807a48f"

	var params []model.ERC20BalanceRpcRequest
	params = append(params, model.ERC20BalanceRpcRequest{
		ContractAddress: "0xc58AD8Ff428c354bb849d1dCf1EDCcAC3F102C8E",
		UserAddress:     "0x8D801a5b332F708d2237784aF230Af7A9863a639",
		ContractDecimal: 18,
	})

	params = append(params, model.ERC20BalanceRpcRequest{
		ContractAddress: "0xc58AD8Ff428c354bb849d1dCf1EDCcAC3F102C8E",
		UserAddress:     "0x81b7E08F65Bdf5648606c89998A9CC8164397647",
		ContractDecimal: 18,
	})

	tag := "latest"
	result, err := eth.NewETHRPCRequest(rpcUrl1).EthGetERC20Balance(params, tag)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	json, _ := json2.Marshal(result)
	t.Log(string(json))
}

func TestEthGetStorageAt(t *testing.T) {
	address := "0x407d73d8a49eeb85d32cf465507dd71d507100c1"
	position := "0x0"
	tag := "latest"
	result, err := eth.NewETHRPCRequest(rpcUrl).EthGetStorageAt(address, position, tag)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestETHGetNonce(t *testing.T) {
	address := "0x407d73d8a49eeb85d32cf465507dd71d507100c1"
	tag := "latest"
	result, err := eth.NewETHRPCRequest(rpcUrl).ETHGetNonce(address, tag)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthGetBlockTransactionCountByHash(t *testing.T) {
	block := "0xd7db0807b7feb1e8e9f7bc7704b5498a7e0da28f44c04fcaf29d07981fb09c73"

	result, err := eth.NewETHRPCRequest(rpcUrl).EthGetBlockTransactionCountByHash(block)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthGetBlockTransactionCountByNumber(t *testing.T) {
	block := "0xe8"

	result, err := eth.NewETHRPCRequest(rpcUrl).EthGetBlockTransactionCountByNumber(block)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthGetUncleCountByBlockHash(t *testing.T) {
	block := "0xd7db0807b7feb1e8e9f7bc7704b5498a7e0da28f44c04fcaf29d07981fb09c73"

	result, err := eth.NewETHRPCRequest(rpcUrl).EthGetUncleCountByBlockHash(block)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthGetUncleCountByBlockNumber(t *testing.T) {
	block := "0xe8"

	result, err := eth.NewETHRPCRequest(rpcUrl).EthGetUncleCountByBlockNumber(block)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthGetCode(t *testing.T) {
	address := "0x704F4eDc12810f554A8Cc4509b278890De201272"
	tag := "latest"
	result, err := eth.NewETHRPCRequest(rpcUrl).EthGetCode(address, tag)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthSign(t *testing.T) {
	address := "0x704F4eDc12810f554A8Cc4509b278890De201272"
	message := "0xdeadbeaf"
	result, err := eth.NewETHRPCRequest(rpcUrl).EthSign(address, message)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthSendRawTransaction(t *testing.T) {
	transaction := "0xd46e8dd67c5d32be8d46e8dd67c5d32be8058bb8eb970870f072445675058bb8eb970870f072445675"
	result, err := eth.NewETHRPCRequest(rpcUrl).EthSendRawTransaction(transaction)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthCall(t *testing.T) {
	gas, _ := strconv.ParseUint("0x76c0", 10, 64)
	gasPrice, _ := strconv.ParseUint("0x9184e72a000", 10, 64)
	transaction := model.CallArgs{
		Data:     "0xd46e8dd67c5d32be8d46e8dd67c5d32be8058bb8eb970870f072445675058bb8eb970870f072445675",
		From:     "0xb60e8dd61c5d32be8058bb8eb970870f07233155",
		Gas:      gas,
		GasPrice: gasPrice,
		To:       "0xd46e8dd67c5d32be8058bb8eb970870f07244567",
		Value:    "0x9184e72a",
		Nonce:    "",
	}

	tag := "latest"
	result, err := eth.NewETHRPCRequest(rpcUrl).EthCall(transaction, tag)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthEstimateGas(t *testing.T) {
	gas, _ := strconv.ParseUint("0x76c0", 10, 64)
	gasPrice, _ := strconv.ParseUint("0x9184e72a000", 10, 64)
	transaction := model.CallArgs{
		Data:     "0xd46e8dd67c5d32be8d46e8dd67c5d32be8058bb8eb970870f072445675058bb8eb970870f072445675",
		From:     "0xb60e8dd61c5d32be8058bb8eb970870f07233155",
		Gas:      gas,
		GasPrice: gasPrice,
		To:       "0xd46e8dd67c5d32be8058bb8eb970870f07244567",
		Value:    "0x9184e72a",
		Nonce:    "",
	}

	tag := "latest"
	result, err := eth.NewETHRPCRequest(rpcUrl).EthEstimateGas(transaction, tag)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	t.Log(result)
}

func TestEthGetBlockByHash(t *testing.T) {
	txHash := "0xdc0818cf78f21a8e70579cb46a43643f78291264dda342ae31049421c82d21ae"
	result, err := eth.NewETHRPCRequest(rpcUrl).EthGetBlockByHash(txHash, false)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	json, _ := json2.Marshal(result)
	t.Log(string(json))
}

func TestEthGetBlockByNumber(t *testing.T) {
	blockNum := "0xbb1f68"
	result, err := eth.NewETHRPCRequest(rpcUrl).EthGetBlockByNumber(blockNum, true)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	json, _ := json2.Marshal(result)
	t.Log(string(json))
}

func TestETHRPCGetTransactionByHash(t *testing.T) {
	txHash := "0x90610b8e972991a521cbb9d806a76e7f65a657716ccf5932d5a86b7e7a3264a5"

	txInfo, err := eth.NewETHRPCRequest(rpcUrl).EthGetTransactionByHash(txHash)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	json, _ := json2.Marshal(txInfo)
	t.Log(string(json))
}

func TestEthGetTransactionByBlockHashAndIndex(t *testing.T) {
	blockHash := "0xd7db0807b7feb1e8e9f7bc7704b5498a7e0da28f44c04fcaf29d07981fb09c73"
	position := "0x0"
	txInfo, err := eth.NewETHRPCRequest(rpcUrl).EthGetTransactionByBlockHashAndIndex(blockHash, position)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	json, _ := json2.Marshal(txInfo)
	t.Log(string(json))
}

func TestEthGetTransactionByBlockNumberAndIndex(t *testing.T) {
	blockNum := "0xbb171f"
	position := "0x0"
	txInfo, err := eth.NewETHRPCRequest(rpcUrl).EthGetTransactionByBlockNumberAndIndex(blockNum, position)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	json, _ := json2.Marshal(txInfo)
	t.Log(string(json))
}

func TestEthGetUncleByBlockHashAndIndex(t *testing.T) {
	blockHash := "0xd7db0807b7feb1e8e9f7bc7704b5498a7e0da28f44c04fcaf29d07981fb09c73"
	position := "0x0"
	txInfo, err := eth.NewETHRPCRequest(rpcUrl).EthGetUncleByBlockHashAndIndex(blockHash, position)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	json, _ := json2.Marshal(txInfo)
	t.Log(string(json))
}

func TestEthGetUncleByBlockNumberAndIndex(t *testing.T) {
	blockNum := "0xbb171f"
	position := "0x0"
	txInfo, err := eth.NewETHRPCRequest(rpcUrl).EthGetUncleByBlockNumberAndIndex(blockNum, position)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	json, _ := json2.Marshal(txInfo)
	t.Log(string(json))
}

func TestSendETHTransaction(t *testing.T) {
	from := "0xDF7B186C2dee907Be7dD9295Da46e681b6FE9e4A"

	to := "0x704F4eDc12810f554A8Cc4509b278890De201272"
	value := "0.002"
	gas := uint64(100000)
	gasPrice := uint64(36000000000)

	err := eth.UnlockETHWallet(keyDir, from, "1234")
	if err != nil {
		t.Error(err.Error())
		return
	}
	txHash, err := eth.NewETHRPCRequest(rpcUrl).EthSendTransaction(model.CallArgs{
		From:     from,
		To:       to,
		Gas:      gas,
		GasPrice: gasPrice,
		Value:    value,
	})
	if err != nil {
		t.Error("send failed: ", err.Error())
		return
	}
	t.Log(txHash)
}

func TestSendERC20ETHTransaction(t *testing.T) {
	from := "0xDF7B186C2dee907Be7dD9295Da46e681b6FE9e4A"
	contract := "0x2AbB7989285B6C72e6067717F5e26697d1Da3627"
	to := "0x704F4eDc12810f554A8Cc4509b278890De201272"
	value := "0.002"
	decimal := 18
	gas := uint64(100000)
	gasPrice := uint64(36000000000)

	err := eth.UnlockETHWallet(keyDir, from, "1234")
	if err != nil {
		t.Error(err.Error())
		return
	}
	txHash, err := eth.NewETHRPCRequest(rpcUrl).EthSendERC20Transaction(from, contract, to, value, gas, gasPrice, decimal)
	if err != nil {
		t.Error("send failed: ", err.Error())
		return
	}
	t.Log(txHash)
}

func TestEthNewBlockFilter(t *testing.T) {
	result, err := eth.NewETHRPCRequest(rpcUrl).EthNewBlockFilter()
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}

	t.Log(result)
}

func TestEthNewPendingTransactionFilter(t *testing.T) {
	result, err := eth.NewETHRPCRequest(rpcUrl).EthNewPendingTransactionFilter()
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}

	t.Log(result)
}

func TestEthUninstallFilter(t *testing.T) {
	filterId := "0x81440f0be92ffa90d8ac1d2468b54d80fc269291e980"
	result, err := eth.NewETHRPCRequest(rpcUrl).EthUninstallFilter(filterId)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}

	t.Log(result)
}

func TestEthGetFilterChanges(t *testing.T) {
	filterId := "0x81440f0beac2bb9d872fafae10fc7b4a9187083a5ff3"
	result, err := eth.NewETHRPCRequest(rpcUrl).EthGetFilterChanges(filterId)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	json, _ := json2.Marshal(result)
	t.Log(string(json))
}

func TestEthGetFilterLogs(t *testing.T) {
	filterId := "0x81440f0beac2bb9d872fafae10fc7b4a9187083a5ff3"
	result, err := eth.NewETHRPCRequest(rpcUrl).EthGetFilterLogs(filterId)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	json, _ := json2.Marshal(result)
	t.Log(string(json))
}

func TestEthGetWork(t *testing.T) {
	result, err := eth.NewETHRPCRequest(rpcUrl).EthGetWork()
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	json, _ := json2.Marshal(result)
	t.Log(string(json))
}

func TestEthSubmitWork(t *testing.T) {
	nonce := "0x0000000000000001"
	powHash := "0x1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef"
	digest := "0xD1FE5700000000000000000000000000D1FE5700000000000000000000000000"

	result, err := eth.NewETHRPCRequest(rpcUrl).EthSubmitWork(nonce, powHash, digest)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}

	if result {
		t.Log("the provided solution is valid")
	} else {
		t.Log("the provided solution is invalid")
	}
}

func TestETHRPCGetTransactions(t *testing.T) {
	txHash1 := "0x90610b8e972991a521cbb9d806a76e7f65a657716ccf5932d5a86b7e7a3264a5"
	txHash2 := "0x1b1773dc9f8b3dfae326824233d949a378cfbd550c1740b2973af6431396b562"
	// invalid, will return {"hash":"",...}
	txHash3 := "0x1b1773dc9f8b3dfae326824233d949a378cfbd550c1740b2973af6431396b563"

	var txHashs []string
	txHashs = append(txHashs, txHash1, txHash2, txHash3)

	if txHashs == nil || len(txHashs) == 0 {
		t.Error("Invalid transaction hash!!!")
		return
	}
	txInfos, err := eth.NewETHRPCRequest(rpcUrl).GetTransactions(txHashs)
	if err != nil {
		t.Error("Query failed: ", err.Error())
		return
	}
	json, _ := json2.Marshal(txInfos)
	t.Log(string(json))
}
