# infura.io

> ID: 1c71b7f01d194ff985ca006da807a48f

# network

```ts
export const NetWorks: { [key: string]: string } = {
  Mainnet: "0x1", // 1
  Ropsten: "0x3", // 3
  Rinkeby: "0x4", // 4
  Goerli: "0x5", // 5
  Kovan: "0x2a", // 42
  Mumbai: "0x13881", // 80001
  Polygon: "0x89", // 137,
};
```

# link

> https: `https://${network}.infura.io/v3/${id}`
> wss: `https://${network}.infura.io/ws/v3/${id}`

# request

```shell
curl https://mainnet.infura.io/v3/1c71b7f01d194ff985ca006da807a48f \
-X POST \
-H "Content-Type: application/json" \
-d '{"jsonrpc":"2.0","method":"eth_blockNumber","params":[],"id":1}'
```

```json
{"jsonrpc":"2.0","id":1,"result":"0x8d8abf"}
```

# https://ropsten.etherscan.io
# api

```
mainnet: https://api.etherscan.io
georli: https://api-goerli.etherscan.io
kovan: https://api-kovan.etherscan.io
rinkeby: https://api-rinkeby.etherscan.io
ropsten: https://api-ropsten.etherscan.io
```