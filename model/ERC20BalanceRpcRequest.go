package model

type ERC20BalanceRpcRequest struct {
	ContractAddress string
	UserAddress     string
	ContractDecimal int
}
