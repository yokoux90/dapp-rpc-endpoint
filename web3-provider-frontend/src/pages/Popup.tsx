import React, { useState } from "react";
import { useLocation } from "react-router-dom";

const Popup = () => {
  const { search } = useLocation();
  const params = new URLSearchParams(search);
  const [counter, setCounter] = useState<number>(0);

  const onClick = () => {
    window.opener.postMessage({ type: "message", counter }, "*");
    window.close();
  };

  return (
    <div className="w-full h-screen flex flex-col justify-center items-center">
      <label>Counter</label>
      <input
        className="border rounded px-1"
        type={"number"}
        value={counter}
        onChange={(e) => setCounter(Number(e.target.value))}
      />
      <br />
      <p>{params.get("msg")}</p>
      <button className="btn" onClick={onClick}>
        Close
      </button>
    </div>
  );
};

export default Popup;
