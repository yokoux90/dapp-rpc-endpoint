import React, { useRef, useState } from "react";
import { BigNumber, providers } from "ethers";
import Web3 from "web3";

const Home = () => {
  const rpcUrl =
    "https://ropsten.infura.io/v3/1c71b7f01d194ff985ca006da807a48f";
  const [counter, setCounter] = useState<number>(0);
  const [address, setAddress] = useState<string>();
  const [balance, setBalance] = useState<string>();
  const [message, setMessage] = useState<string>();

  const onClick = () => {
    window.open(
      "/popup?msg=hello",
      "p2",
      "width=300,height=600,menubar=no,toolbar=no, status=no,scrollbars=yes",
    );

    window.addEventListener("message", (e) => {
      setCounter(e.data.counter ?? 0);
    });
  };


  const onFetchClick = async () => {
    const web3 = new Web3(rpcUrl);
    const provider = new providers.Web3Provider(web3.givenProvider);
    const signer = provider.getSigner(
      "0x704F4eDc12810f554A8Cc4509b278890De201272",
    );

    setAddress(await signer.getAddress());

    const balance: BigNumber = await signer.getBalance();

    setBalance(balance.toString());

    // 没有指定地址和解锁的过程所以这里跑不过
    // 预想的是这里弹出popup,交给后台处理再返回这里
    const msg = await signer.signMessage("hello");
    setMessage(msg);
  };

  return (
    <div className="grid grid-cols-3 gap-4">
      <div className="border rounded">
        <label>Popup Test</label>
        <p>{counter}</p>
        <button className="btn" onClick={onClick}>
          Popup
        </button>
      </div>
      <div className="border rounded">
        <label>Fetch Address Test</label>
        <ul>
          <li>{address}</li>
          <li>{balance}</li>
          <li>{message}</li>
        </ul>
        <button className="btn" onClick={onFetchClick}>
          Fetch
        </button>
      </div>
    </div>
  );
};

export default Home;
