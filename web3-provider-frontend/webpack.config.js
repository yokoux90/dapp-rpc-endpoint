const { resolve } = require("path");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  mode: "development",
  entry: resolve(__dirname, "./src/index.tsx"),
  output: {
    path: resolve(__dirname, "./dist"),
    filename: "bundle.js",
  },
  resolve: {
    alias: {
      src: resolve(__dirname, "./src"),
      components: resolve(__dirname, "./src/components"),
      utils: resolve(__dirname, "./src/utils"),
    },
    extensions: [".js", ".jsx", ".ts", ".tsx", ".json"],
  },
  module: {
    rules: [
      {
        test: /\.s?css$/,
        use: ["style-loader", "css-loader", "postcss-loader", "sass-loader"],
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: ["babel-loader", "ts-loader"],
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg|mp4|ogg|avi|mp3)$/i,
        use: ["url-loader"],
      },
    ],
  },
  devtool: "eval-cheap-module-source-map",
  cache: {
    type: "filesystem",
  },
  devServer: {
    hot: true,
    open: true,
    historyApiFallback: true,
    compress: true,
    port: 9000,
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: resolve(__dirname, "./index.html"),
    }),
    new CleanWebpackPlugin(),
  ],
};
